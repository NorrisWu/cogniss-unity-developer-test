﻿namespace Developer.Core
{
    using UnityEngine;

    /// <inheritdoc />
    /// <summary>
    /// Controller which loads all required plugins before loading the first screen for the app.
    /// </summary>
    public sealed class PlatformLauncher : MonoBehaviour
    {
        // What's the use of this variable in this particular test tho?
        // I hope it's alright for me to change it into a variable;
        [SerializeField] bool UseReporter = false;

        private App _app = null;

        private void Awake()
        {
            if (UseReporter)
            {
                Reporter _reporter = gameObject.GetComponent<Reporter>();

                //Add "Reporter" Component to gameObject after added "UnityLogsViewer" to Development Assembly Definition References.
                if (_reporter == null)
                    _reporter = gameObject.AddComponent<Reporter>();
            }

            _app = new App(GameObject.Find("Canvas"));
            _app.ChangeScreen<Developer.Screen.LoadingScreen>();
        }
    }
}