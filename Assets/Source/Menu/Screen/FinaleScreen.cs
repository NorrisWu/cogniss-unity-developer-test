﻿namespace Developer.Screen
{
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.SceneManagement;

    /// <inheritdoc />
    /// <summary>
    /// Controller for the screen that display certain background based on the device in use, and with Function that restart the app/>.
    /// </summary>
    class FinaleScreen : Core.ScreenAbstract
    {
        [SerializeField] 

        private void Start()
        {
            Button _restartBtn = transform.Find("RestartButton").GetComponent<Button>();
            if (_restartBtn != null)
                _restartBtn.onClick.AddListener(RestartApplication);

            SetupBackgroundColor();
        }
        
        // Setup Background color of main camera based on the platform
        private void SetupBackgroundColor()
        {
            Camera.main.backgroundColor = new Color(0.2f, 0.2f, 0.2f, 1);

            #if UNITY_EDITOR
                Camera.main.backgroundColor = Color.yellow;
            #endif

            #if UNITY_ANDROID
                Camera.main.backgroundColor = Color.red;
            #endif

            #if UNITY_IOS
                Camera.main.backgroundColor = Color.green;
            #endif
        }

        public void RestartApplication()
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }
    }
}