﻿namespace Developer.Screen
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;
    using System.Threading.Tasks;

    /// <inheritdoc />
    /// <summary>
    /// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
    /// </summary>
    public sealed class LightScreen : Core.ScreenAbstract
    {
        private Developer.Component.TrafficLight lights = null;

        private void Awake()
        {
            GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
            GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
            lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
            lights.SetState();
        }

        //TODO: According the rules, turn the lights on and off...
        //Hint: The networking functionality is inside the "Test" namespace.
        
        [Tooltip("How many times do you want to change light value? I think?")]
        [SerializeField] int m_RequiredIteration = 100;
        [SerializeField] int m_LightValue;
        [Tooltip("Timer for delay between each light switch (in milliseconds)")]
        [SerializeField] int m_LightDelay = 3000;

        [SerializeField] Text m_CountdownTimer;

        private void Start()
        {
            m_CountdownTimer = transform.Find("CountdownTimer").GetComponent<Text>();

            // Find FinaleButton in the child, then link LoadFinaleScreen() to OnClick Listener.
            Button _finaleBtn = transform.Find("FinaleButton").GetComponent<Button>();
            if (_finaleBtn != null)
                _finaleBtn.onClick.AddListener(LoadFinaleScreen);

            // Running Traffic Light Function Async
            SwitchingLightAsync();
        }

        /*
         * Traffic Light Switching Controller
         * Start this function will reset iteration. m_LightValue will be updated for m_RequiredIteration times, 
         * if value is multiple of 15 then turn red, if value is multiple of 5 then turn yellow, if value is multiple of 3         
         * turn greem. In all other condition, light will turn off. Light Will stay on for m_LightTimer second.         
         */

        #region Tasks System Version
        async void SwitchingLightAsync()
        {
            for (int i = 0; i < m_RequiredIteration; i++)
            {
                if (lights != null && gameObject != null)
                {
                    if (m_LightValue != 0)
                    {
                        if ((m_LightValue % 15) == 0)
                        {
                            lights.SetState(Test.LightColor.Green);
                            InitiateCountdownTimer();
                            await Task.Delay(m_LightDelay);
                        }
                        else if ((m_LightValue % 5) == 0)
                        {
                            lights.SetState(Test.LightColor.Yellow);
                            InitiateCountdownTimer();
                            await Task.Delay(m_LightDelay);
                        }
                        else if ((m_LightValue % 3) == 0)
                        {
                            lights.SetState(Test.LightColor.Red);
                            InitiateCountdownTimer();
                            await Task.Delay(m_LightDelay);
                        }
                        else
                            lights.SetState();
                    }

                    //GetLightValue();  <== This doesn't work.... but why??
                    m_LightValue = await Test.NetworkService.GetCurrentLightValue();
                }
            }

            if (_app != null)
                _app.ChangeScreen<FinaleScreen>();

        }
        #endregion

        async void InitiateCountdownTimer()
        {
            for (int i = (m_LightDelay / 1000); i >= 0; i--)
            {
                if (m_CountdownTimer != null)
                    m_CountdownTimer.text = (i).ToString("0");
                await Task.Delay(1000);
            }
        }

        // If I use this, it won't work?
        private async void GetLightValue()
        {
            m_LightValue = await Test.NetworkService.GetCurrentLightValue();
        }

        // Go to FinaleScreen
        public void LoadFinaleScreen()
        {
            _app.ChangeScreen<FinaleScreen>();
        }

    }
}